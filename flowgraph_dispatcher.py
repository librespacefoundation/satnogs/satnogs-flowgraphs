#!/usr/bin/env python3

from argparse import ArgumentParser
import signal

class FlowgraphDispatcher():
    """Generate & execute SatNOGS Flowgraphs

    :param parameters: Flowgraph parameters parsed with argparse
    """

    def __init__(cls, parameters):
        cls.parameters = parameters
        cls.hier_blocks = []

def client_argument_parser():
    """Initialize argument parser for satnogs-client arguments."""
    parser = ArgumentParser()
    parser.add_argument("--antenna", help="Set SoapySDR device antenna name for RX")
    parser.add_argument("--baud", help="For compatibility with client args. NOT USED!")
    parser.add_argument("--bb-freq", help="Set SoapySDR device baseband CORDIC frequency", type=float)
    parser.add_argument("--bw", help="Set SoapySDR device RF filter bandwidth", type=float)
    parser.add_argument("--dc-removal", help="Set SoapySDR automatic DC removal", type=int)
    parser.add_argument("--decoded-data-file-path", help="Set decoded frames filepath")
    parser.add_argument("--dev-args", help="Set SoapySDR device arguments")
    parser.add_argument("--doppler-correction-per-sec", help="Set number of Doppler corrections per sec", type=float)
    parser.add_argument("--enable-iq-dump", help="Enable/Disable iq data dumps", type=bool)
    parser.add_argument("--file-path", help="Set the audio recordings file path")
    parser.add_argument("--framing", help="For compatibility with client args. NOT USED!")
    parser.add_argument("--gain", help="Set SoapySDR device overall gain", type=float)
    parser.add_argument("--gain-mode", help="Set SoapySDR device gain mode")
    parser.add_argument("--iq-file-path", help="Set the iq dumps file path")
    parser.add_argument("--lo-offset", help="Set SoapySDR local oscillator offset")
    parser.add_argument("--other-settings", help="Set SoapySDR channel other settings")
    parser.add_argument("--ppm", help="Set SoapySDR device oscillator frequency correction in ppm", type=float)
    parser.add_argument("--rigctl-host", help="Set Hamlib rotctld host IP")
    parser.add_argument("--rigctl-port", help="Set Hamlib rigctld port", type=int)
    parser.add_argument("--rx-freq", help="Set SoapySDR device center frequency", type=float)
    parser.add_argument("--samp-rate-rx", help="Set SoapySDR device sampling rate", type=float)
    parser.add_argument("--soapy-rx-device", help="Set SoapySDR device options")
    parser.add_argument("--stream-args", help="Set SoapySDR device stream args")
    parser.add_argument("--tune-args", help="Set SoapySDR device tune args")
    parser.add_argument("--udp-dump-host", help="Set UDP Doppler corrected destination IP")
    parser.add_argument("--udp-dump-port", help="Set UDP Doppler corrected destination port", type=int)
    parser.add_argument("--waterfall-file-path", help="Set waterfall data file path")
    parser.add_argument("--wpm", help="For compatibility with client args. NOT USED!")
    
    return parser

def main():
    #####################################
    # Argument parser
    ####################################
    args = client_argument_parser().parse_args()
    
    flowgraph_d = FlowgraphDispatcher(args)

if __name__ == '__main__':
    main()
