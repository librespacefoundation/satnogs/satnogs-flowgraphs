options:
  parameters:
    author: Manolis Surligas (surligas@gmail.com)
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: Decoder for the QUBIK 1 & 2 satellites
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: satnogs_qubik_telem
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: satnogs_qubik_telem
    window_size: 2*1080,1080
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 20]
    rotation: 0
    state: enabled

blocks:
- name: alpha
  id: variable
  parameters:
    comment: ''
    value: 0.5*1
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [440, 716.0]
    rotation: 0
    state: enabled
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1560, 28.0]
    rotation: 0
    state: enabled
- name: decimation
  id: variable
  parameters:
    comment: ''
    value: max(4,satnogs.find_decimation(baudrate, 2, audio_samp_rate))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1352, 28.0]
    rotation: 0
    state: true
- name: variable_cc_decoder_def_0
  id: variable_cc_decoder_def
  parameters:
    comment: ''
    dim1: '1'
    dim2: '1'
    framebits: (128+32+4)*8
    k: '7'
    mode: fec.CC_STREAMING
    ndim: '0'
    padding: 'False'
    polys: '[79, -109]'
    rate: '2'
    state_end: '-1'
    state_start: '0'
    value: '"ok"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [968, 1172.0]
    rotation: 0
    state: enabled
- name: variable_ieee802_15_4_variant_decoder_0
  id: variable_ieee802_15_4_variant_decoder
  parameters:
    comment: ''
    crc_type: CRC32_C
    custom_crc: satnogs.crc.crc16(0x1021, 0xFFFF, 0xFFFF, True, True)
    drop_invalid: 'True'
    frame_len: 128+32
    preamble: '[0x33]*4'
    preamble_thrsh: '12'
    rs: 'True'
    sync_thrsh: '3'
    sync_word: '[0x3C, 0x67, 0x49, 0x52]'
    var_len: 'False'
    whitening: satnogs.whitening.make_ccsds(True)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1136, 1172.0]
    rotation: 0
    state: true
- name: variable_ieee802_15_4_variant_decoder_cc
  id: variable_ieee802_15_4_variant_decoder
  parameters:
    comment: "Allow more wrong bits on the sync word,\n for the Viterbi paths to settle"
    crc_type: CRC32_C
    custom_crc: satnogs.crc.crc16(0x1021, 0xFFFF, 0xFFFF, True, True)
    drop_invalid: 'True'
    frame_len: 128+32
    preamble: '[0x33]*4'
    preamble_thrsh: '12'
    rs: 'True'
    sync_thrsh: '7'
    sync_word: '[0x3C, 0x67, 0x49, 0x52]'
    var_len: 'False'
    whitening: satnogs.whitening.make_ccsds(True)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1320, 1172.0]
    rotation: 0
    state: true
- name: analog_quadrature_demod_cf_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.2'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1072, 652.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '0.9'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1064, 516.0]
    rotation: 0
    state: enabled
- name: antenna
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 28.0]
    rotation: 0
    state: enabled
- name: baudrate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '9600'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1464, 28.0]
    rotation: 0
    state: enabled
- name: bb_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Baseband CORDIC frequency (if the device supports it)
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1856, 156.0]
    rotation: 0
    state: enabled
- name: blocks_complex_to_interleaved_short_0
  id: blocks_complex_to_interleaved_short
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    scale_factor: '1.0'
    vector_output: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [248, 460.0]
    rotation: 0
    state: enabled
- name: blocks_file_sink_0
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: iq_file_path
    type: short
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [760, 436.0]
    rotation: 0
    state: enabled
- name: blocks_multiply_const_vxx_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: 1/(math.pi*1.1)
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1616, 900.0]
    rotation: 180
    state: true
- name: blocks_null_sink_0
  id: blocks_null_sink
  parameters:
    affinity: ''
    alias: ''
    bus_structure_sink: '[[0,],]'
    comment: ''
    num_inputs: '1'
    type: short
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [760, 408.0]
    rotation: 0
    state: enabled
- name: blocks_selector_0
  id: blocks_selector
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    enabled: 'True'
    input_index: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '1'
    num_outputs: '2'
    output_index: enable_iq_dump
    showports: 'True'
    type: short
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 436.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1216, 28.0]
    rotation: 0
    state: enabled
- name: dc_blocker_xx_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1320, 644.0]
    rotation: 0
    state: enabled
- name: dc_blocker_xx_0_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1480, 508.0]
    rotation: 0
    state: enabled
- name: dc_removal
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Remove automatically the DC offset (if the device support it)
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1704, 156.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/.satnogs/data/data"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 1052.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [352, 28.0]
    rotation: 0
    state: enabled
- name: digital_binary_slicer_fb_0
  id: digital_binary_slicer_fb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1448, 776.0]
    rotation: 180
    state: enabled
- name: digital_clock_recovery_mm_xx_0
  id: digital_clock_recovery_mm_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain_mu: 0.5/8.0
    gain_omega: 2 * math.pi / 100
    maxoutbuf: '0'
    minoutbuf: '0'
    mu: '0.5'
    omega: '2'
    omega_relative_limit: '0.01'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1504, 620.0]
    rotation: 0
    state: enabled
- name: digital_fll_band_edge_cc_0
  id: digital_fll_band_edge_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filter_size: (audio_samp_rate// baudrate)*2+1
    maxoutbuf: '0'
    minoutbuf: '0'
    rolloff: alpha
    samps_per_sym: audio_samp_rate/ baudrate
    type: cc
    w: 2*math.pi/(audio_samp_rate/ baudrate)/100
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 568.0]
    rotation: 0
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [168, 1052.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [264, 972.0]
    rotation: 0
    state: enabled
- name: fec_extended_decoder_0
  id: fec_extended_decoder
  parameters:
    affinity: ''
    alias: ''
    ann: None
    comment: ''
    decoder_list: variable_cc_decoder_def_0
    maxoutbuf: '0'
    minoutbuf: '0'
    puncpat: '''11'''
    threadtype: capillary
    value: fec_extended_decoder
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1400, 876.0]
    rotation: 180
    state: true
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"test.wav"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [152, 972.0]
    rotation: 180
    state: enabled
- name: gain
  id: parameter
  parameters:
    alias: ''
    comment: "Generic gain value. \nEach Soapy module\nwill try to linearize this\n\
      value by properly setting\nthe device specific gains."
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1072, 28.0]
    rotation: 0
    state: enabled
- name: gain_mode
  id: parameter
  parameters:
    alias: ''
    comment: 'Set the gain mode of the Soapy.

      Can be "Overall", "Specific"

      or "Settings Field"'
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"Overall"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [904, 28.0]
    rotation: 0
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [0, 172.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/iq.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 972.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 28.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: 0.625 * baudrate
    decim: decimation // 2
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 8.0
    win: window.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [784, 556.0]
    rotation: 0
    state: enabled
- name: network_udp_sink_0_0
  id: network_udp_sink
  parameters:
    addr: udp_dump_host
    affinity: ''
    alias: ''
    comment: ''
    header: '0'
    payloadsize: '1472'
    port: udp_dump_port
    send_eof: 'False'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [760, 1220.0]
    rotation: 0
    state: enabled
- name: other_settings
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Channel other settings
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1552, 156.0]
    rotation: 0
    state: enabled
- name: pfb_arb_resampler_xxx_1
  id: pfb_arb_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    atten: '80'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    nfilts: '32'
    rrate: audio_samp_rate /( (baudrate*decimation) / (decimation // 2))
    samp_delay: '0'
    taps: ''
    type: fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1240, 492.0]
    rotation: 0
    state: true
- name: ppm
  id: parameter
  parameters:
    alias: ''
    comment: 'The frequency correction in PPM

      to correct for a local oscillator frequency deviation'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1680, 28.0]
    rotation: 0
    state: enabled
- name: rigctl_host
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"127.0.0.1"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 1148.0]
    rotation: 0
    state: enabled
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 1068.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [640, 28.0]
    rotation: 0
    state: enabled
- name: samp_rate_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [488, 28.0]
    rotation: 0
    state: enabled
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '1'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: baudrate*decimation
    samp_rate: samp_rate_rx
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 196.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ieee802_15_4_variant_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1080, 776.0]
    rotation: 180
    state: enabled
- name: satnogs_frame_decoder_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ieee802_15_4_variant_decoder_cc
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1080, 904.0]
    rotation: 180
    state: true
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: '''{"Coding":"RS"}'''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [864, 796.0]
    rotation: 180
    state: true
- name: satnogs_json_converter_0_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: '''{"Coding":"CC-RS"}'''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [872, 924.0]
    rotation: 180
    state: true
- name: satnogs_ogg_encoder_0
  id: satnogs_ogg_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filename: file_path
    quality: '1.0'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1624, 500.0]
    rotation: 0
    state: enabled
- name: satnogs_waterfall_sink_0
  id: satnogs_waterfall_sink
  parameters:
    affinity: ''
    alias: ''
    center_freq: rx_freq
    comment: ''
    fft_size: '1024'
    filename: waterfall_file_path
    mode: '1'
    rps: '10'
    samp_rate: baudrate*decimation
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [24, 684.0]
    rotation: 180
    state: enabled
- name: snippet_0
  id: snippet
  parameters:
    alias: ''
    code: self.satnogs_doppler_compensation_0.variable_rigctl.set_param("rig_pathname",
      str(self.rigctl_host)+ ":"+str(self.rigctl_port) )
    comment: Convenient way to support arbitrary rigctl sources
    priority: '0'
    section: main_after_init
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [712, 148.0]
    rotation: 0
    state: enabled
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"driver=invalid"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [224, 28.0]
    rotation: 0
    state: enabled
- name: soapy_source_0_0
  id: soapy_source
  parameters:
    affinity: ''
    agc0: 'False'
    agc1: 'False'
    alias: ''
    amp_gain0: '0'
    ant0: antenna
    ant1: RX2
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: ppm
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_removal0: dc_removal
    dc_removal1: 'True'
    dev: soapy_rx_device
    dev_args: ''
    devname: custom
    gain_mode0: gain_mode
    gain_mode1: Overall
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    nchan: '1'
    nco_freq0: bb_freq
    nco_freq1: '0'
    overall_gain0: gain
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rf_gain0: '18'
    rfgr_gain: '9'
    rxvga1_gain: '5'
    rxvga2_gain: '0'
    samp_rate: samp_rate_rx
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    settings0: other_settings
    settings1: ''
    stream_args: stream_args
    tia_gain0: '0'
    tia_gain1: '0'
    tune_args0: tune_args
    tune_args1: ''
    tuner_gain0: '10'
    type: fc32
    vga_gain0: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [192, 164.0]
    rotation: 0
    state: true
- name: stream_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Stream arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1216, 156.0]
    rotation: 0
    state: enabled
- name: tune_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Channel Tune arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1392, 156.0]
    rotation: 0
    state: enabled
- name: udp_dump_host
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"127.0.0.1"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [560, 1108.0]
    rotation: 0
    state: enabled
- name: udp_dump_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '57356'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [688, 1108.0]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [728, 236.0]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [24, 612.0]
    rotation: 0
    state: true
- name: virtual_source_0_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [424, 1252.0]
    rotation: 0
    state: enabled
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/waterfall.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 972.0]
    rotation: 0
    state: enabled
- name: zeromq_pub_msg_sink_1_0_0
  id: zeromq_pub_msg_sink
  parameters:
    address: zmq_pub_uri
    affinity: ''
    alias: ''
    bind: 'True'
    comment: ''
    timeout: '100'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [560, 852.0]
    rotation: 180
    state: enabled
- name: zmq_pub_uri
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: tcp://127.0.0.1:16887
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [592, 956.0]
    rotation: 0
    state: enabled

connections:
- [analog_quadrature_demod_cf_0_0, '0', dc_blocker_xx_0, '0']
- [analog_quadrature_demod_cf_0_0_0, '0', pfb_arb_resampler_xxx_1, '0']
- [blocks_complex_to_interleaved_short_0, '0', blocks_selector_0, '0']
- [blocks_multiply_const_vxx_0, '0', fec_extended_decoder_0, '0']
- [blocks_selector_0, '0', blocks_null_sink_0, '0']
- [blocks_selector_0, '1', blocks_file_sink_0, '0']
- [dc_blocker_xx_0, '0', digital_clock_recovery_mm_xx_0, '0']
- [dc_blocker_xx_0_0, '0', satnogs_ogg_encoder_0, '0']
- [digital_binary_slicer_fb_0, '0', satnogs_frame_decoder_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', blocks_multiply_const_vxx_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', digital_binary_slicer_fb_0, '0']
- [digital_fll_band_edge_cc_0, '0', low_pass_filter_0, '0']
- [fec_extended_decoder_0, '0', satnogs_frame_decoder_0_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0_0_0, '0']
- [pfb_arb_resampler_xxx_1, '0', dc_blocker_xx_0_0, '0']
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0, out, satnogs_json_converter_0, in]
- [satnogs_frame_decoder_0_0, out, satnogs_json_converter_0_0, in]
- [satnogs_json_converter_0, out, zeromq_pub_msg_sink_1_0_0, in]
- [satnogs_json_converter_0_0, out, zeromq_pub_msg_sink_1_0_0, in]
- [soapy_source_0_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', blocks_complex_to_interleaved_short_0, '0']
- [virtual_source_0, '0', digital_fll_band_edge_cc_0, '0']
- [virtual_source_0, '0', satnogs_waterfall_sink_0, '0']
- [virtual_source_0_0, '0', network_udp_sink_0_0, '0']

metadata:
  file_format: 1
